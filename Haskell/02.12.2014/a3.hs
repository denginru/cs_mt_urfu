import Data.Maybe

recip' :: Double -> Maybe Double
recip' x = 
  if x == 0 then Nothing else Just (recip x)

log' :: Double -> Maybe Double
log' x =
  if x <= 0 then Nothing else Just (log x)  

asin' :: Double -> Maybe Double
asin' x =
  if abs x > 1 then Nothing else Just (asin x) 

sqrt' :: Double -> Maybe Double
sqrt' x = 
  if x <= 0 then Nothing else Just (sqrt x)

(*$*) :: (a -> Maybe a) -> Maybe a -> Maybe a  
(*$*) g x = 
  if isJust x then g $ fromJust x
  else Nothing
  
infixr *$*
  
f' x = 
  sqrt' *$* asin' *$* log' *$* recip' *$* Just x

f'' x = 
  Just x <$> recip' <$> log' <$> asin' <$> sqrt'
 
  
  
  
  
  
  
  
  
  
  
  
  








		