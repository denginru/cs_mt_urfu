data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Eq, Show, Ord)

addElem :: Ord a => Tree a -> a -> Tree a
addElem Empty element = (Node element) Empty Empty
addElem (Node a left right) element
    | element < a     = Node a (addElem left element) right
    | element > a     = Node a left (addElem right element)
    | otherwise 	  = (Node a left right)

contains :: Ord a => Tree a -> a -> Bool
contains Empty element = False
contains (Node a left right) element
	| element == a = True
	| element < a  = contains left element
	| otherwise    = contains right element

count :: Tree a -> Int
count Empty = 0
count (Node a Empty Empty) = 1
count (Node a left right) =
	count left + count right + 1

sumElems :: Num a => Tree a -> a
sumElems Empty = 0
sumElems (Node a Empty Empty) = a
sumElems (Node a left right) = a + sumElems left + sumElems right

prodElems :: Num a => Tree a -> a
prodElems Empty = 0
prodElems (Node a Empty Empty) = a
prodElems (Node a left right) = a * prodElems left * prodElems right