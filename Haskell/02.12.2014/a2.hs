import Data.Maybe

f x =
  if x == 0 then error "1/"
  else let x1 = 1/x in
    if x1 < 0 then error "log"
	else let x2 = log x1 in
	  if abs x2 > 1 then error "asin"
	  else let x3 = asin x2 in
	    if x3 < 0 then error "sqrt"
		else sqrt x3
		
recip' :: Maybe Double -> Maybe Double
recip' Nothing = Nothing		
recip' (Just x) = 
  if x == 0 then Nothing else Just (recip x)

log' :: Maybe Double -> Maybe Double
log' Nothing = Nothing		
log' (Just x) =
  if x <= 0 then Nothing else Just (log x)  

asin' = undefined

sqrt' = undefined

f' x = sqrt' $ asin' $ log' $ recip' (Just x)












		