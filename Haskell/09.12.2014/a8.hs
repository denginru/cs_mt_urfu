import System.IO

main :: IO()
main = do
  inData <- readFile "input.txt"
  let
    a:b:_ = map read $ words inData :: [Int]
    res = a+b
	
  putStrLn $ show res
  