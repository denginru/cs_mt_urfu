import Data.Functor
import Control.Applicative

z1 :: [Int] -> [Int -> Int]
z1 lst = (+) <$> lst

z3 :: [Int] -> [Int] -> [(Int, Int)]
z3 lst1 lst2 = (,) <$> lst1 <*> lst2