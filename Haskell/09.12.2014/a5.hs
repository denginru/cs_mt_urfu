import Control.Monad.State

fact :: Int -> State Int Int
fact n = 
  if n <= 0 then return 1
  else factHelp''' 1
    where 
	  factHelp :: Int -> State Int Int
	  factHelp k = do
		  prod <- get
		  if k == n then return (k*prod)
		  else do
			put (k*prod)
			factHelp (k+1)
			
	  factHelp' k =	
	    get >>=
		  (\prod -> 
		    if k == n then return (k*prod)
		    else 
			  put (k*prod) >> factHelp' (k+1))
			  
	  factHelp'' k = do
		  prod <- get
		  let
		    prod' = k*prod
		  if k == n then return prod'
		  else do
			put prod'
			factHelp'' (k+1)
		
	  factHelp''' k =	
	    get >>=
		  (\prod -> 
		    let
			  prod' = prod*k
			in
		      if k == n then return (k*prod)
		      else 
			    put (k*prod) >> factHelp''' (k+1))
		
		
		
		
		
		
		
		
			
			
			
			
			