import Control.Monad.Writer

half' :: Int -> (Int,String)
half' n =
  if even n then (n `div` 2, "Halved " ++ show n)
  else (n, "Cannot half " ++ show n)
  
half :: Int -> Writer String Int
half n = do
  tell ("Halved " ++ (show n) ++ "! ")
  return (n `div` 2)

half'' :: Int -> Writer String Int
half'' n = 
  tell ("Halved " ++ (show n) ++ "! ")
    >> return (n `div` 2)
	
	
	
	
	
	
	
	
	