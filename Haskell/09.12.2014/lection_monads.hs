import  Control.Monad.Writer
import  Control.Monad.State
import  Data.List
--half'' :: Int -> Writer 
{-half'' n =
	tell ("Halved " ++ (show n) ++ "!")
	>> return (n `div` 2)-}

fact :: Int -> State Int Int
fact n =
	if n <= 0 then return 1
	else factHelp 1
		where
			factHelp :: Int -> State Int Int
			factHelp k = do
				prod <- get
				if k == n  then  return (k*prod)
				else do
					put (k*prod)
					factHelp (k+1)

f lst a = map (+a) lst
g lst a = fmap (+a) lst
h lst a = do
	b <- lst
	return (a+b)

h' :: [Int] -> Int -> [Int]
h' lst a = concatMap (\b -> return (a+b)) lst


gggg l1 l2 = do
	a <- l1
	b <- l2
	return (a*b)