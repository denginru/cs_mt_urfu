data TSuit =
	Heart | Diamond | Spades | Baptize
	deriving (Eq,Ord)

data TRang =
	Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Valet | Queen | King | Ace
	deriving (Eq,Ord)

{--rangToInt :: TRang -> Integer
rangToInt rang = 
	case rang of
		Two -> 2
		Three -> 3
		Four -> 4
		Five -> 5
		Six -> 6
		Seven -> 7
		Eight -> 8
		Nine -> 9
		Ten -> 10
		Valet -> 11
		Queen -> 13
		King -> 14
		Ace -> 15--}

data TCard = Card
	{suit :: TSuit, rang :: TRang}

isMinor :: TCard -> Bool
isMinor card = rang card < Valet --((rangToInt $ rang card) >= 2) && ((rangToInt $ rang card) <= 10)

sameSuit :: TCard -> TCard -> Bool
sameSuit card1 card2 = ((suit card1)==(suit card2))