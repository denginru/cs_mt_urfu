data TSuit =
	Heart | Diamond | Spades | Baptize
	deriving (Eq,Ord, Show)

data TRang =
	Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Valet | Queen | King | Ace
	deriving (Eq,Ord, Show)

{--rangToInt :: TRang -> Integer
rangToInt rang = 
	case rang of
		Two -> 2
		Three -> 3
		Four -> 4
		Five -> 5
		Six -> 6
		Seven -> 7
		Eight -> 8
		Nine -> 9
		Ten -> 10
		Valet -> 11
		Queen -> 13
		King -> 14
		Ace -> 15--}

data TCard = Card
	{suit :: TSuit, rang :: TRang}

instance Show TCard where
	show card = "(" ++ show (rang card) ++ "," ++ show (suit card) ++ ")"

isMinor :: TCard -> Bool
isMinor card = rang card < Valet --((rangToInt $ rang card) >= 2) && ((rangToInt $ rang card) <= 10)

sameSuit :: TCard -> TCard -> Bool
sameSuit card1 card2 = ((suit card1)==(suit card2))

beats :: TCard -> TCard -> Bool
beats card1 card2 =
	((suit card1) == (suit card2)) && ((rang card1) > (rang card2))

beats2 :: TCard -> TCard -> TSuit -> Bool
beats2 card1 card2 trump =
	if (suit card2 /= trump) then
		case (suit card1) of 
			trump -> True
			otherwise -> ((suit card1) == (suit card2)) && ((rang card1) > (rang card2))
	else
		((suit card1) == (suit card2)) && ((rang card1) > (rang card2))

beatList :: [TCard] -> TCard -> TSuit -> [TCard]
beatList lst card trump
	| (suit card) == trump = filter (\x -> ((suit x)==trump) && ((rang x)>(rang card))) lst
	| otherwise = filter (\y -> ((suit y)==(suit card) && (rang y) > (rang card)) || ((suit y) == trump)) (filter (\x -> ((suit x)==(suit card))||((suit x)==(trump))) lst)