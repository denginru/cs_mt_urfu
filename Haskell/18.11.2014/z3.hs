sumSeries :: (Int -> Rational) -> Int -> Rational -> Rational
sumSeries f n0 eps = foldl (+) 0 $ takeWhile ((>= eps) . abs) $ map f [1..]