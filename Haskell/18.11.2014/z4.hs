whatN :: Int

whatN = length $ takeWhile (< 0.9999) (map (sin) (map (^2) [1..]))