data Bool' = False' | True'
  deriving (Show, Eq, Ord)

False' && _ = False'
_ && False' = False'
_ && _ = True'
