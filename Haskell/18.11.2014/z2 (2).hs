listToDoubleEvenList :: [Int] -> [Int]

listToDoubleEvenList lst = concatMap (check) lst where
	check h =
		if even n
			[h,h]
		else
			[h]
