data TCartesian = Cartesian 
  { x::Double, y::Double }
--  deriving (Show,Eq)
  deriving (Eq,Ord)
  
instance Show TCartesian where
	show p = "(" ++ show (x p) ++ ";" ++
	  show (y p) ++ ")"

instance Num TCartesian where
 (+) p1 p2 = Cartesian 
      (x p1 + x p2) (y p1 + y p2)
 (-) p1 p2 = Cartesian 
      (x p1 - x p2) (y p1 - y p2)
 (*) _ _ = undefined
 signum _ = undefined
 abs _ = undefined
 fromInteger _ = undefined

(><) :: Double -> TCartesian  -> TCartesian
(><) a (Cartesian x0 y0) = Cartesian (a*x0) (a*y0)
 
 
 

