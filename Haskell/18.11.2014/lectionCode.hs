{-type Cartesian = (Double, Double)
type Polar = (Double, Double)

a :: Polar
a = (1,1)

f::Cartesian -> Double
f (x,y) = sqrt (x*x + y*y)

data TCartesian = Cartesian Double Double
f :: Cartesian -> Double
f (Cartesian x y) = sqrt (x^2+y^2)

-}

data TCartesian = Cartesian 
	{x::Double, y::Double}
	deriving (Eq, Ord)

--Тип геом. формы: точка, окружность, прямоугольник

data TShape =
	Point TCartesian | Circle TCartesian Double | Rectangle TCartesian TCartesian

square1 :: TShape -> Double
square1 (Point _)                    = 0
square1 (Circle _ r)                 = pi*r^2
square1 (Rectangle 	(Cartesian x1 y1)
					(Cartesian x2 y2)) = abs (x1-x2) * abs (y1-y2)

square2 :: TShape -> Double
square2 s = case s of
	Point _         -> 0
	Circle _ r      -> pi*r^2
	Rectangle p1 p2 -> abs (x1-x2) * abs (y1-y2)
		where
			Cartesian x1 y1 = p1
			Cartesian x2 y2 = p2

--Пример запуска: square1 (Point (Cartesian 1 5965))

x0 :: TShape -> Double
x0 (Circle (Cartesian res _) _) = res

instance Show TCartesian where
	show p = "(" ++ show (x p) ++ "; " ++ show (y p) ++ ")"

instance Num TCartesian where
	(+) p1 p2 = Cartesian (x p1 + x p2) (y p1 - y p2)
	(-) p1 p2 = Cartesian (x p1 - x p2) (y p1 - y p2)
	(*) _ _   = undefined
	abs _     = undefined