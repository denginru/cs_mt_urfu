triangularNums :: Int -> [Int]
--Общая формула n-ого треугольного числа: T_n = n*(n+1)/2
triangularNums n = take n $ map (\x-> x * (x+1) `div` 2) [1..]