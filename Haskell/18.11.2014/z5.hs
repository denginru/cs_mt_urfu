digitsOfAllNums :: Int -> [Int]

digitsOfAllNums n = concatMap numToList [1..n] where
	numToList n = reverse (map (`mod` 10) (takeWhile (>0) (iterate (`div` 10) n)))