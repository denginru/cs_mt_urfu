data TCartesian = Cartesian 
  { x::Double, y::Double }
--  deriving (Show,Eq)
  deriving (Eq,Ord)
  
instance Show TCartesian where
	show p = "(" ++ show (x p) ++ ";" ++
	  show (y p) ++ ")"
	  
-- ��� �������������� �����: �����, ����, �������.
data TShape =
  Point TCartesian |
  Circle { center::TCartesian, radius::Double} |
  Rectangle {v1::TCartesian, v2::TCartesian}
  
square :: TShape -> Double
square s = case s of
  Point _ -> 0
  Circle _ _ -> pi* radius s^2
  Rectangle _ _ -> 
    abs ((x $ v1 s) - (x $ v2 s)) * 
	 abs ((y $ v1 s) - (y $ v2 s))
