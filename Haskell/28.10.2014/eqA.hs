eq a b c =
	let
		d=4*a*c
		sd = sqrt(d)
		x1=((-b)+sd)/(2*a)
		x2=((-b)-sd)/(2*a)
	in
		if d<0 then
			error "No roots"
		else
			if d == 0
				x1
			else
				if (x1>x2) then x1 else x2
			