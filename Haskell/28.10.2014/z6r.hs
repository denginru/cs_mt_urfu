root :: (Double -> Double) -> Double -> Double -> Double -> Double
find :: (Double -> Double) -> Double -> Double -> Double -> Double -> Double

find f a b eps r = 
	if f r == 0 then
		r
	else
		if (b-a)<eps then
			(b+a)/2.0
		else
			if (((f a < 0) && (f r < 0)) || ((f a > 0) && (f r > 0))) then
				find f r b eps ((r+b)/2.0)
			else
				find f a r eps ((a+r)/2.0)

root (f) a b eps = find f a b eps ((b+a)/2.0)