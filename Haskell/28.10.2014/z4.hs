revNum :: Int -> Int

iter :: Int -> Int -> Int
iter c n = 
	if c == 0 then
		n
	else
		iter (div c 10) (n*10+(mod c 10))
revNum c = 
	if c < 10 then
		c
	else
		iter c 0 