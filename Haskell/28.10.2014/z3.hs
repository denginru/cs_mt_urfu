isPrime :: Int -> Bool

check :: Int -> Int -> Bool
check n d =
	if n == d then
		True
	else
		if (mod n d) == 0 then
			False
		else
			check n (d + 1)

isPrime 2 = True
isPrime n = if n < 2 then
				False
			else
				check n 2