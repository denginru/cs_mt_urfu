eq :: Int -> Int -> Int -> Double

eq a b c =
	|d==0 = (-b)/(2*a)
	|d>0 = max(x1, x2)
	|otherwise = error "Нет корней"
	where
		d = b*b-4.0*a*c
		x1 = ((-b)-(sqrt d))/(2*a)
		x2 = ((-b)+(sqrt d))/(2*a)