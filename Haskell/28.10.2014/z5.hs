solveEq :: Int -> Int -> Int -> Double
solveEq a b c =
	let
		d = b*b-4*a*c
		s = (sqrt (fromIntegral d))
		x1 = ((fromIntegral(-b))+s)/(fromIntegral(2*a))
		x2 = ((fromIntegral(-b))-s)/(fromIntegral(2*a))
	in
		if d<0 then
			error "Корней нет"
		else
			max x1 x2