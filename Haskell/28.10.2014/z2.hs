mygcd :: Int -> Int -> Int

mygcd x y =
	if (x /= 0) && (y /= 0) then
		if x>0 then
			gcd (mod x y) y
		else
			gcd x (mod y x)
	else
		x+y