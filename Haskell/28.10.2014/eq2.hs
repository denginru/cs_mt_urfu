eq :: Int -> Int -> Int -> Double

eq a b c =
	|d<0 = error "No roots"
	|d==0 = (-b)/(2.0*a)
	|d>0 = max x1 x2 where
		x1 = ((-b)-(sqrt (b*b - 4.0*a*c)))/(2*a)
		x2 = ((-b)+(sqrt (b*b - 4.0*a*c)))/(2*a)