import  Data.List

fileSort = do
	info <- readFile "input.txt"
	writeFile "output.txt" $ unwords $ map show $ 
	  sortBy (flip compare) $ map (read :: String -> Int) $ words info
	--(foldl (++) "" show  (reverse $ sort $ map (\x -> read x :: Int) (words info)))