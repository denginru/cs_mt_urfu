sumSquaresRec :: (Num a) => [a] -> a
sumSquaresIter :: (Num a) => [a] -> a
sumSquaresH :: (Num a) => [a] -> a
iter :: (Num a) => [a] -> a -> a

sumSquaresRec [a] = a*a
sumSquaresRec (h:t) = h*h+(sumSquaresRec t)

sumSquaresIter lst = iter lst 0

iter [] r = r
iter (h:t) r = iter t (r+h*h)

sumSquaresH lst = 
	foldl (+) 0 $ map (\x->x*x) lst