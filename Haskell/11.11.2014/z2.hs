combine :: [a] -> [a] -> [a]
dosort :: [a] -> [a] -> [a] -> [a]
combine lst1 lst2 = dosort lst1 lst2 []

dosort lst1 lst2 res	| (null lst1) && (null lst2) = res
						| null lst1 = dosort lst1 t2 [h2:res]
						| null lst2 = dosort t1 lst2 [h1:res]
						| otherwise =
							if h1<h2 then
								dosort t1 lst2 [h1:res]
							else
								dosort lst1 t2 [h2:res]
						where
							[h1:t1] = lst1
							[h2:t2] = lst2