myzip :: ([a], [b]) -> [(a,b)]

myzip Pair = iter (fst Pair) (snd Pair) [] where
	iter lst1 lst2 res
		| ((null lst1) || (null lst2)) = res
		| 