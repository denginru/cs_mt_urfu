listToNum :: [Integer] -> Integer

listToNum lst = iter (reverse lst) 0 where
	iter lst r
		| null lst  = 0
		| null t    = r*10+h
		| otherwise = iter t r*10+h
		where
			h = head lst
			t = tail lst