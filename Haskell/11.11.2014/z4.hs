findSum :: Int -> Double

findSum n =
	foldl (+) 0 (take n lst)
	where
		lst = [1/(fromIntegral (i^2)) | i <- [1..]]