strangeCompare :: Integer -> Integer -> Ordering
intToList :: Integer -> [Integer]
compareLists :: [Integer] -> [Integer] -> Ordering
listToNum :: [Integer] -> Integer

listToNum lst = iter (reverse lst) 0 where
	iter lst r
		| null lst  = 0
		| null t    = 10*r+h
		| otherwise = iter t r*10+h
		where
			h = head lst
			t = tail lst

intToList a = iter a [] where
	iter a lst
		| (a<10)	=(a:lst)
		| otherwise = iter (a `div` 10) ((a `mod` 10):lst)



compareLists lst1 lst2 =
	compare (listToNum (filter even lst1)) (listToNum (filter even lst2))
strangeCompare x y = compareLists (intToList x) (intToList y)