fact :: Integer->Integer
fact 1 = 1
fact x = x * fact(x-1)


myabs :: Integer -> Integer
myabs x =
	if x > 0 then
		x
	else
		(-x)