#include <stdio.h>
#include <stdlib.h>
int *GenArray(int a, int b, int n)
{
    int i;
    int *ar;
    ar = (int*) malloc(n*sizeof(int));
    for (i = 0; i < n; i++)
        /*ar[i] = (int)(a+(double)rand()*(b-a)/RAND_MAX);*/
        ar[i] = rand()%(b-a)+a;
    return ar;
}

int main()
{
    int n, i;
    double a,b;
    int *ar;
    printf("Введите к-во элементов массива: ");
    scanf("%d", &n);
    printf("a, b=");
    scanf("%lf%lf", &a, &b);
    ar = GenArray(a,b,n);
    printf("%d\n", n);
    for (i = 0; i<n; i++)
        printf("%d\n", ar[i]);
    free(ar);
    return 0;
}
