#include <stdio.h>
#include <stdlib.h>

//Возвращает к-во символов в строке
int StringSize(char *s)
{
    int i=0;
    while (s[i] != '\0')
        i++;
    return i;
}

char *ChangeStringSize(char *s, int l)
{
    int i, n;
    char *res;
    n = StringSize(s);
    res = (char *) malloc(l * sizeof(char));
    if (n < l) //Если реальный размер строки меньше, чем надо, добиваем строку *-ми
    {
        for (i=0; i<n; i++)
            res[i]=s[i];
        for (i=n; i<l; i++)
            res[i]='*';
    }
    else    //Иначе обрезаем
        for (i=0; i<l; i++)
            res[i]=s[i];
    return res;
}

int main()
{
    int n;
    char s[200];
    printf("Длина = ");
    scanf("%d", &n);
    printf("Строка = ");
    gets(s);
    /*t = ChangeStringSize(s,n);*/
    printf("%s\n", ChangeStringSize(s,n));
    return 0;
}