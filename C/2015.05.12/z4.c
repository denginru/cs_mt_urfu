#include <stdio.h>

void sort(unsigned char *a, int l)
{
	int k[3], i;
	k[0] = 0;
	k[1] = 0;
	k[2] = 0;
	for (i=0; i<l; i++)
		k[a[i]-1]++;
	for (i=0; i<k[0]; i++)
		a[i]=1;
	for (i=k[0]; i<k[1]+k[0]; i++)
		a[i]=2;
	for (i=k[1]+k[0]; i<k[2]+k[1]+k[0]; i++)
		a[i]=3;
}

int main()
{
	int *ar;
	int i,n;
	printf("Введите к-во элементов массива: ");
    scanf("%d", &n);
    ar = (int*) malloc(n*sizeof(int));
    for (i=0; i<n; i++)
    {
    	printf("1, 2 or 3? ");
    	scanf("%d", &ar[i]);
    }
    sort(ar, n);
    for (i=0; i<n; i++)
    	printf("%d\n", ar[i]);
    return 0;
}