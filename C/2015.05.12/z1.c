#include <stdio.h>
#include <limits.h>

void MinMaxArray(int *A, int len, int *min, int *max)
{
    if (len > 0)
    {
        int _min = INT_MAX, _max = INT_MIN, i;
        for (i = 0; i<len; i++)
        {
            if (A[i]<_min) _min = A[i];
            if (A[i]>_max) _max = A[i];
        }
        *min = _min;
        *max = _max;
    }
    else
    {
        *min = INT_MIN;
        *max = INT_MAX;
    }
}

int main()
{
    int b[10] = {1,2,3,4,5,6,7,8,9,10};
    int min, max;
    MinMaxArray(b, 10, &min, &max);
    printf("%d\n%d\n", min, max);
    return 0;
}
