#include <stdio.h>
#include <stdlib.h>
void ReverseArray(int *a, int l)
{
	int i, t;
	for (i = 0; i < l/2; i++)
	{
		t=a[i];
		a[i] = a[l-i-1];
		a[l-i-1] = t;
	}
}

int *GenArray(int a, int b, int n)
{
    int i;
    int *ar;
    ar = (int*) malloc(n*sizeof(int));
    for (i = 0; i < n; i++)
        ar[i] = rand()%(b-a)+a;
    return ar;
}

int main()
{
	int *ar;
	int i,n;
	double a,b;
	printf("Введите к-во элементов массива: ");
    scanf("%d", &n);
    printf("a, b=");
    scanf("%lf%lf", &a, &b);
    ar = GenArray(a,b,n);
    for (i=0; i<n; i++)
    	printf("%d\n", ar[i]);
    printf("------------------\n");
    ReverseArray(ar, n);
    for (i=0; i<n; i++)
    	printf("%d\n", ar[i]);
    return 0;
}
