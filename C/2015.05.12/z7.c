#include <stdio.h>

int StringSize(char *s)
{
	int i=0;
	while (s[i] != '\0')
		i++;
	return i;
}

char *code(char *str)
{
	int l = StringSize(str), i=0, k=0, size = 3*l-2;
	const int a = 32;
	const int b = 127;
	char *res;
	res = (char *) malloc(size * sizeof(char));
	while (str[i] != '\0')
	{   
		res[k] = str[i];
		if (str[i+1] != '\0') res[k+1] = (char)(rand()%(b-a)+a);
        if (str[i+1] != '\0') res[k+2] = (char)(rand()%(b-a)+a);
		i++;
		k=k+3;
	}
	res[k]='\0';
	return res;
}

char *decode(char *str)
{
	int i=0, l=StringSize(str);
	char *res;
	res = (char *) malloc(l * sizeof(char)/3+1);
	for (i=0; i<l; i=i+3)
		res[i/3]=str[i];
    res[i]='\0';
	return res;
}

int main()
{
	char s[200];
	srand(time(0));
	printf("Input a string for coding: ");
	gets(s);
	printf("%s\n", code(s));
	printf("Input a string for decoding: ");
	gets(s);
	printf("%s\n", decode(s));
	return 0;
}
