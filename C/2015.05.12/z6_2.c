#include <stdio.h>
#include <stdlib.h>

int StringSize(char *s)
{
    int i=0;
    while (s[i] != '\0')
        i++;
    return i;
}

char *ChangeStringSize(char *s, int l)
{
    int i, n;
    char *res;
    n = StringSize(s);
    res = (char *) malloc(l * sizeof(char) + 1);
    if (n < l)
    {
        for (i=0; i<n; i++)
            res[i]=s[i];
        for (i=n; i<l; i++)
            res[i]='*';
    }
    else
        for (i=0; i<l; i++)
            res[i]=s[i];
    printf("%d\n", i);
    res[i]='\0';
    return res;
}

int main()
{
    int n = 13;
    char s[200];
    printf("Строка = ");
    gets(s);
    printf("Длина = ");
    scanf("%d", &n);
    printf("%s\n", ChangeStringSize(s, n));
    return 0;
}