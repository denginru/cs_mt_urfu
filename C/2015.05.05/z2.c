#include <stdio.h>

int NOD(int a, int b)
{
  int temp;
  while (b != 0)
  {
  	 temp = b;
  	 b = a % b;
  	 a = temp; 
  }

  return a;
/*
	while (A!=0 && B!=0)
		if (A>B) 
			A%=B;
		else
			B%=A;
	return A+B;
*/
}

int NOK(int a, int b)
{
	return abs(a*b) / NOD(a, b);
}

int main()
{
	int a, b;
	printf("Введите 2 числа\n");
	scanf("%d%d", &a, &b);
	printf("НОД = %d\nНОК = %d\n", NOD(a,b), NOK(a,b));
	return 0;
}