#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int main()
{
	int a, b, n, i, c;
	double min, max, s=0;
	srand(time(0));
	printf("Количество чисел: \n");
	scanf("%d", &n);
	printf("Введите 2 целых числа - диапозон: ");
	scanf("%d%d", &a, &b);
	min = b; max = a;
	for (i = 0; i < n; i++)
	{
		c = (int)(a+(double)rand()*(b-a)/RAND_MAX);
		printf("%d ", c);
		if (c>max) max = c;
		if (c<min) min = c;
		s+=c;
	}
	printf("\nМинимум = %f\nМаксимум = %f\nСумма = %f\n", min, max, s);
	return 0;
}