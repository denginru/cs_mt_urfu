#include <stdio.h>

int main()
{
	int n,m,a,b,i;
	double d;
	printf("n,m, a, b = ");
	scanf("%d%d%d%d", &n, &m, &a, &b);
	if (m>n)
	{
		d = (b-a)/(m-n);
		for (i=a; i<=b; i+=d)
		{
			printf("%d\n", i);
		}
	}
	else
	{
		d = (a-b)/(n-m);
		for (i=a; i>=b; i-=d)
		{
			printf("%d\n", i);
		}
	}
}
