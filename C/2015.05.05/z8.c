#include <stdio.h>
#include <math.h>



int main()
{
	int i, n, s=0, sc=0;
	double max;
	printf("Input a real number: ");
	scanf("%d", &n);
	
	for (i=1; i<=n*(n+1)/2; i++)
		if (sin((double)i) > max) {max = sin((double)i); s = i;}

	printf("Maximum is %f. Sum is %d\n", max, s);
	printf("This numbers give a maximum sin of sum:\n");
	
	while (s != sc)
	{
		if (s-sc > n)
		{
			printf("%d ", n);
			sc+=n;
		}
		else
		{
			printf("%d ", s-sc);
			sc=s;
		}
		n--;
	}
	printf("\n");
	return 0;
}