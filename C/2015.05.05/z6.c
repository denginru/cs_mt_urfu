#include <stdio.h>
#include <math.h>

int roots(double a, double b, double c, double *x1, double *x2)
{
	double d = b*b-4*a*c;
	int k;
	if (d>0)
	{
		k=2;
		*x1 = (-b+sqrt(d))/(2*a);
		*x2 = (-b-sqrt(d))/(2*a);
	}
	else
		if (d == 0)
		{
			*x1 = (-b)/(2*a);
			*x2 = (-b)/(2*a);	
			k=1;
		}
		else
		{
			k=0;
		}
	return k;
}

int main()
{
	int k;
	double a, b, c;
	double x1, x2;
	printf("Введите коэффициенты a, b и c\n");
	scanf("%lf%lf%lf", &a, &b, &c);
	k = roots(a, b, c, &x1, &x2);
	printf("К-во корней = %d\n", k);
	if (k>0) printf("Корни %f %f\n",x1, x2);
	return 0;
}