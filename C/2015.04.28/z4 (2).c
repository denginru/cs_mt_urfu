#include <stdio.h>

int main()
{
	float r, a, ar, S, L;
	scanf("%f%f", &r, &a);
	ar = a*3.14/180;
	L = r * ar;
	S = r * L / 2;
	printf("Длина дуги = %f \nПлощадь = %f\n", L, S);
	return 0;
}