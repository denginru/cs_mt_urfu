#include <stdio.h>

struct Time
{
    int hourse, minute, second;
};

void PrintTime(struct Time time)
{  
    printf("%d:%d:%d\n", time.hourse, time.minute, time.second);
}

struct Time IncTime(struct Time time, int seconds)
{
    int h=0, m=0, s=0, p=0;
    struct Time rTime;
    s=time.second+seconds;
    if (s>=60) {s%=60; p=seconds/60;}
    m=time.minute + p;
    if (m>=60) {p=m/60; m%=60;} else p=0;
    h=time.hourse + p;
    if (h>=24) h%=24;
    rTime.hourse = h; rTime.minute = m; rTime.second = s;
    return rTime;
}

int CmpTime(struct Time time1, struct Time time2)
{
    int t1=3600*time1.hourse+60*time1.minute+time1.second, t2=3600*time2.hourse+60*time2.minute+time2.second;
    if (t1>t2)
        return 1;
    else
        if (t1<t2)
            return -1;
        else
            return 0;
}

void SwapTime(struct Time *time1, struct Time *time2)
{
    struct Time t;
    t = *time1;
    *time1 = *time2;
    *time2 = t;
}

int main()
{
    struct Time t1, t2;
    t1.hourse = 23; t1.minute = 59; t1.second = 50;
    t2.hourse = 7; t2.minute = 20; t2.second = 0;
    PrintTime(t1);
    PrintTime(t2);
    printf("----------\n");
    SwapTime(&t1, &t2);
    PrintTime(t1);
    PrintTime(t2);
    printf("----------\n");
    printf("%d\n", CmpTime(t1, t2));
    PrintTime(IncTime(t2, 360));
    return 0;
}