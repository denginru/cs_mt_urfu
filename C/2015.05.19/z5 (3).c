/*
    ЗАДАЧА:
        1) Отсортировать набор по к-ву страниц
        2) Найти медиану страниц
        3) Указать автора, у кого больше всего книг в данном наборе
*/

#include <stdio.h>
#include <string.h>
struct Book
{
    char author[100], title[1000];
    int  count_of_pages;
};

/*Сортируем массив по к-ву страниц*/
void Sort(struct Book *books, int n)
{
    int i, j;
    struct Book t;
    for (i=0; i<n; i++)
        for (j=i; n-1; j++)
            if ( books[j].count_of_pages < books[j+1].count_of_pages)
            {
                t = books[j];
                books[j] = books[j+1];
                books[j+1] = t;
            }
}

/*Возвращаем медиану*/
int Mediana(struct Book *books)
{
    
}

void Parse(char *s, char *author, char *title, int *count_of_pages)
{
    char n[20];
    int i=0, c=0;
    
    while (s[i] != ' ')
    {
        author[c]=s[i];
        c++;
        i++;
    }
    author[i] = '\0';
    i++;
    c=0;
    
    while (s[i] != ' ')
    {
        title[c]=s[i];
        c++;
        i++;
    }
    title[i]='\0';
    i++;
    c=0;
    
    while (s[i] != '\0')
    {
        n[c]=s[i];
        c++;
        i++;
    }
    
    *count_of_pages = atoi(n);
}

int main()
{
	struct Book books[1000];
    FILE *mf;
    char str[1200];
    char au[100], ti[1000];
    char *estr;
    int i = 0, k=0, t=1;
    mf = fopen("books.txt", "r");
    if (mf == NULL) {printf("error");return 0;}
	
    while (1)
    {
		estr = fgets (str,sizeof(str),mf);
		if (estr == NULL)
			break;
        str[strlen(str)-1]='\0';
        Parse(str, au, ti, &(books[i].count_of_pages));
        strcpy(books[i].author, au);
        strcpy(books[i].title, ti);
        i++;
    }    
    
    k=i;
    
    for (i = 0; i < k; i++)
        printf("%s %s %d\n", books[i].author, books[i].title, books[i].count_of_pages);
    
    return 0;
}