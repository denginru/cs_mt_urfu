#include <stdio.h>  

int main (void)
{  

   FILE *mf;

   char str[50];


   char *estr;


   printf ("Open file: ");
   mf = fopen ("books.txt","r");


   if (mf == NULL) {printf ("Error\n"); return -1;}
   else printf ("Done\n");

   printf ("Strings:\n");


   while (1)
   {

      estr = fgets (str,sizeof(str),mf);


      if (estr == NULL)
      {

         if ( feof (mf) != 0)
         {  

            printf ("\nEnd of reading\n");
            break;
         }
         else
         {

            printf ("\Error\n");
            break;
         }
      }

      printf ("     %s",str);
   }


   printf ("Close file: ");
   if ( fclose (mf) == EOF) printf ("error\n");
   else printf ("done\n");

   return 0;
}