#include <stdio.h>

int main()
{
    int n, t, i;
    double x0, a, c, s=0;
    FILE *f;
    f = fopen("poly.txt", "r");
    fscanf(f, "%lf%d", &x0, &n);
    fscanf(f, "%lf", &a);
    s=a;
    for (i=1; i<=n; i++)
    {
        fscanf(f, "%lf", &a);
        s=x0*s+a;
    }
    printf("%f\n", s);
    return 0;
}