#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main()
{
    int n, t, i, s=0, min=INT_MAX, max = INT_MIN;
    FILE *f;
    f = fopen("randnum.txt", "r");
    fscanf(f, "%d", &n);
    for (i=0; i<n; i++)
    {
        fscanf(f, "%d", &t);
        s+=t;
        if (t<min) min = t;
        if (t>max) max = t;
    }
    printf("Max = %d\n Min = %d\n Sum = %d\n", min, max, s);
    return 0;
}
