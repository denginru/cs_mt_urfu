#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n, a, b, i;
    FILE *f;
    f = fopen("randnum.txt", "w");
    printf("Введите n, a, b");
    scanf("%d%d%d", &n, &a, &b);
    fprintf(f, "%d\n", n);
    for (i=0; i< n;i++)
        fprintf(f, "%d ", rand()%(b-a+1)+a);
    fclose(f);
    return 0;
}
