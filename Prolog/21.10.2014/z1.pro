opposite(left,right).
opposite(right,left).

unsafe(places(M, F, Son1, Son2, _, _, _, _)) :-
    %M = Son1, M = Son2, opposite(M, F);
    M = Son1, opposite(M, Son2), opposite(M, F);
    M = Son2, opposite(M, Son1), opposite(M, F).
    
unsafe(places(M, F, _, _, D1, D2, _, _)) :-
    %F = D1, F = D2, opposite(M, F);
    F = D1, opposite(F, D2), opposite(M, F);
    F = D2, opposite(F, D2), opposite(M, F).
    
unsafe(places(_, _, _, _, _, _, P, A)) :-
    opposite(P, A).
    
unsafe(places(M, F, Son1, Son2, D1, D2, P, A)) :-
    opposite(P, A), M = A;
    opposite(P, A), F = A;
    opposite(P, A), Son1 = A;
    opposite(P, A), Son2 = A;
    opposite(P, A), D1 = A;
    opposite(P, A), D2 = A.

move(places(M1, F, Son1, Son2, D1, D21, P, A), places(M2, F, Son1, Son2, D1, D22, P, A)) :- opposite(M1, M2), M1 = D11, M2 = D12.
move(places(M1, F, Son1, Son2, D1, D21, P, A), places(M2, F, Son1, Son2, D1, D22, P, A)) :- opposite(M1, M2), M1 = D21, M2 = D22.
move(places(M, F1, Son11, Son2, D1, D2, P, A), places(M, F2, Son12, Son2, D1, D2, P, A)) :- opposite(F1, F2), F1 = S11, F2 = S12.
move(places(M, F1, Son1, Son21, D1, D2, P, A), places(M, F2, Son1, Son22, D1, D2, P, A)) :- opposite(F1, F2), F1 = S21, F2 = S22.
move(places(M1, F1, Son1, Son2, D1, D2, P, A), places(M2, F2, Son1, Son2, D1, D2, P, A)) :- opposite(F1, F2), F1 = M1, F2 = M2.
move(places(M, F, Son1, Son2, D1, D2, P1, A1), places(M, F, Son1, Son2, D1, D2, P1, A1)) :- opposite(P1, P2), P1 = A1, P2 = A2.
move(places(M, F, Son1, Son2, D1, D2, P1, A), places(M, F, Son1, Son2, D1, D2, P2, A)) :- opposite(P1, P2).
move(places(M1, F, Son1, Son2, D1, D2, P, A), places(M2, F, Son1, Son2, D1, D2, P, A)) :- opposite(M1, M2).
move(places(M, F1, Son1, Son2, D1, D2, P, A), places(M, F2, Son1, Son2, D1, D2, P, A)) :- opposite(F1, F2).
move(places(M, F, Son11, Son2, D1, D2, P1, A), places(M, F, Son12, Son2, D1, D2, P2, A)) :- opposite(P1, P2), P1 = Son11, P2 = Son12.
move(places(M, F, Son1, Son21, D1, D2, P1, A), places(M, F, Son1, Son22, D1, D2, P2, A)) :- opposite(P1, P2), P1 = Son21, P2 = Son22.
move(places(M, F, Son1, Son2, D11, D2, P1, A), places(M, F, Son2, Son2, D12, D2, P2, A)) :- opposite(P1, P2), P1 = D11, P2 = D12.
move(places(M, F, Son1, Son2, D1, D21, P1, A), places(M, F, Son1, Son2, D1, D22, P2, A)) :- opposite(P1, P2), P1 = D21, P2 = D22.

findPath(Finish,Path,Finish,Path).
findPath(Current,Path,Finish,ResPath) :-
  move(Current, Next),
  \+ memberchk(Next, Path),
  \+ unsafe(Next),
  findPath(Next,[Next|Path],Finish,ResPath).

go :-
  Start = places(left,left,left,left,left,left,left,left),
  Finish = places(right,right,right,right,right,right,right,right),
  findPath(Start,[Start],Finish, ResPath),
  printPath(ResPath).
  
printPath([_]) :- !, nl.
printPath([H1,H2|T]) :-
  printPath([H2|T]),
  printElem(H1,H2).
  
printElem(places(M1, F, Son1, Son2, D1, D2, P, A), places(M2, F, Son1, Son2, D1, D2, P, A))
    :- write('Мать переехала с '),
       write(M1),
       write('на '),
       write(M2), nl.

printElem(places(M1, F, Son1, Son2, D11, D2, P, A), places(M2, F, Son1, Son2, D12, D2, P, A))
    :- write('Мать переехала с дочерью №1 с'),
       write(M1),
       write('на '),
       write(M2), nl.

printElem(places(M1, F, Son1, Son2, D1, D21, P, A), places(M2, F, Son1, Son2, D1, D22, P, A))
    :- write('Мать переехала с дочерью №2 с'),
       write(M1),
       write('на '),
       write(M2), nl.

printElem(places(M, F1, Son1, Son2, D1, D2, P, A), places(M, F2, Son1, Son2, D1, D2, P, A))
    :- write('Отец переехала с '),
       write(F1),
       write('на '),
       write(F2), nl.

printElem(places(M, F1, Son11, Son2, D1, D2, P, A), places(M, F2, Son12, Son2, D1, D2, P, A))
    :- write('Отец переехала с сыном №1 с'),
       write(F1),
       write('на '),
       write(F2), nl.

printElem(places(M, F1, Son1, Son21, D1, D2, P, A), places(M, F2, Son1, Son22, D1, D2, P, A))
    :- write('Отец переехала с сыном №2 с'),
       write(F1),
       write('на '),
       write(F2), nl.

printElem(places(M, F, Son1, Son2, D1, D2, P1, A), places(M, F, Son1, Son2, D1, D2, P2, A))
    :- write('Полицейский переехала с '),
       write(P1),
       write('на '),
       write(P2), nl.

printElem(places(M, F, Son1, Son2, D1, D2, P1, A1), places(M, F, Son1, Son2, D1, D2, P2, A2))
    :- write('Полицейский переехала с '),
       write(P1),
       write('на '),
       write(P2), nl.

printElem(places(M, F, Son1, Son2, D1, D2, P1, A), places(M, F, Son1, Son2, D1, D2, P2, A))
    :- write('Полицейский переехала с арестантом с'),
       write(F1),
       write('на '),
       write(F2), nl.

printElem(places(M, F, Son11, Son2, D1, D2, P1, A), places(M, F, Son12, Son2, D1, D2, P2, A))
    :- write('Полицейский переехала с сыном №1'),
       write(P1),
       write('на '),
       write(P2), nl.

printElem(places(M, F, Son1, Son21, D1, D2, P1, A), places(M, F, Son1, Son22, D1, D2, P2, A))
    :- write('Полицейский переехала с сыном №2'),
       write(P1),
       write('на '),
       write(P2), nl.

printElem(places(M, F, Son1, Son2, D11, D2, P1, A), places(M, F, Son1, Son2, D12, D2, P2, A))
    :- write('Полицейский переехала с дочерью №1'),
       write(P1),
       write('на '),
       write(P2), nl.

printElem(places(M, F, Son1, Son2, D1, D21, P1, A), places(M, F, Son1, Son2, D1, D22, P2, A))
    :- write('Полицейский переехала с дочерью №2'),
       write(P1),
       write('на '),
       write(P2), nl.