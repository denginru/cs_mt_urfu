even(X):-(X mod 2) =:= 0.
filter(List1,List2,FiltFunc) :- myfilt(List1, [], List2, FiltFunc).

myfilt([], Lst1, Lst2, _) :- reverse(Lst1, Lst), Lst=Lst2, !.
myfilt(Lst1, Temp, Lst2, F) :-
	[Head|Tail] = Lst1, call(F, Head), T=[Head|Temp], myfilt(Tail, T, Lst2, F);
	[Head|Tail] = Lst1, myfilt(Tail, Temp, Lst2, F).