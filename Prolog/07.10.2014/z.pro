initialization(consult('kinship.pro')).

pred(X, Y) :-
    setof(W, parent(W, X), Lst), member(A, Lst), parent(A, Y);
    setof(W, parent(W, X), Lst), member(A, Lst), pred(A, Y).
    
% !!! Не работает сопоставление !!!    
brother(X, X) :- !, fail.
brother(X, Y) :- male(X), male(Y), setof(W, parent(W, X), Lst1), setof(W, parent(W, Y), Lst2), Lst2 = Lst1.


num_of_children(X, N) :- setof(Y, parent(X, Y), Lst), length(Lst, N).
