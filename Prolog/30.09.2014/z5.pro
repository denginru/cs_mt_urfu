included(Lst1, Lst2) :- Lst1 = Lst2.
included(Lst1, Lst2) :- length(Lst1, X), length(Lst2, Y), X = Y, Lst1 =\= Lst2, !, fail.
                        
