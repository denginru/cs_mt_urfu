%myReverse(Lst, Lst).
%myReverse(Lst1, Lst2) :- myrev(Lst1, [], Lst2).
%myrev(Lst1, Temp, List2) :- write(Lst1), write(' '), write(Temp), write(' '), write(Lst2), nl.
%myrev(Lst1, Temp, List2) :- 
%                            List1 = [], Temp = List2;
%                            [Head|Tail] = List1, append(Temp, [Head|[]], NewTemp), myrev(Tail, NewTemp, List2).

myReverse(Lst1, Lst2) :- length(Lst2, L2), length(Lst1, L1), L1 \= L2, !, fail.
myReverse(Lst1, Lst2) :- myrev(Lst1, [], Lst2).
myrev(Lst1, Temp, Lst2) :-  
							Temp = Lst2;
							[Head|Tail] = Lst1, X = [Head|[]], append(X, Temp, NewTemp) ,myrev(Tail, NewTemp, Lst2).