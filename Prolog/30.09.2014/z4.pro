z4(Lst1, Lst2) :- mycheck(Lst1, [], Lst2).

is_prime(1) :- !.
is_prime(X) :- prime(X, 2).
prime(2, _).
prime(X, X).
prime(X, Y) :- (X mod Y) =:= 0, !, fail;
               (X mod Y) =\= 0, Y1 is Y + 1, prime(X, Y1).

mycheck(Lst1, Temp, Lst2) :- Lst1 = [], Temp = Lst2, !.
mycheck(Lst1, Temp, Lst2) :- [Head|Tail] = Lst1, is_prime(Head), X = [Head|[]], append(Temp, X, NewTemp), mycheck(Tail, NewTemp, Lst2);
                             [Head|Tail] = Lst1, \=(is_prime(Head), true), mycheck(Tail, Temp, Lst2).
