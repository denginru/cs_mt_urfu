repeat1.
repeat1 :- repeat1.

:- dynamic(counter/1).
counter(0).

print10 :-
    repeat1,
    retract(counter(X)),
    X is X + 1,
    write(X1), nl,
    asserta(counter(X1)),
    X1 = 10, !,
    abolish(counter/1).
