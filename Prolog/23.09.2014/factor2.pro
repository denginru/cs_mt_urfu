factor(0, 1) :- !.
factor(1, 1) :- !.
factor(2, 2) :- !.
factor(N, _) :- N < 1, !, fail.
factor(N, Res) :- N mod 2 =:= 0, fact_even(N, Res);
                  N mod 2 =\= 0, fact_odd(N, Res).
                  
fact_even(2, 2) :- !.
fact_even(N, _) :- N < 2, !, fail.
fact_even(N, Res) :- N1 is N - 2, fact_even(N1, Res1), Res is Res1 * N.

fact_odd(1, 1) :- !.
fact_odd(N, _) :- N < 1, !, fail.
fact_odd(N, Res) :- N1 is N - 2, fact_odd(N1, Res1), Res is Res1 * N.
