siracuz(0, _)  :- !.
siracuz(N, A0) :- NN is N - 1, sir(0, NN, A0).
sir(Count, Count, A) :- write(A), nl.
sir(Index, Count, A) :- A mod 2 =:= 0,   write(A), nl, NN is Index + 1, AA is A // 2, sir(NN, Count, AA);
                        A mod 2 =\= 0, write(A), nl, NN is Index + 1, AA is (3 * A + 1), sir(NN, Count, AA).
