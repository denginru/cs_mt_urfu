%Вычисление факториала
factor(N, Res) :- N < 0, !, fail.
factor(0, 1).
factor(1, 1).
factor(N, Res) :- N1 is (N - 1),
                  factor(N1, Res1), 
                  Res is N * Res1.
