is_prime(1) :- !, fail.
is_prime(X) :- prime(X, 2).
prime(2, _).
prime(X, X).
prime(X, Y) :- (X mod Y) =:= 0, !, fail;
               (X mod Y) =\= 0, Y1 is Y + 1, prime(X, Y1).
