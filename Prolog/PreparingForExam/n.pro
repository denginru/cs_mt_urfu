n([], _, 0) :- !.
n([H|T], K, N) :- Temp is mod(H, K), Temp = 0, n(T, K, N1), N is N1 + 1;
				  Temp is mod(H, K), Temp > 0, n(T, K, N1), N is N1.