factorail(0, 1)   :- !.
factorail(N, Res) :- N1 is (N-1),
				 factorail(N1, Res1),
				 Res is N * Res1.