dropWhile([], _, []) :- !.
dropWhile([H1|T1], Pred, [H2|T2]) :-
	call(Pred, H1), H2 is H1, dropWhile(T1, Pred, T2);
	call(Pred, H1), H2 =\= H1, fail, !.


even(X) :- 0 is mod(X, 2).