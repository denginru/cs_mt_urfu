.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd 5
	b dd 7
	d dd 17

.CODE
main:
	mov eax, a
	mul b
	div d
	call exit
end main