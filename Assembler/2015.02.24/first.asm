.486


.MODEL Flat, StdCall

; Это некоторые мантры
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	av dd 1
	bv dd 2
	cv dd ?

.CODE
main:
	mov eax, a
	mov ebx, b
	add eax, ebx
	mov cv, eax
	call exit
end main