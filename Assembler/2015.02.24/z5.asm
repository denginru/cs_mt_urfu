.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd 486
	b dd 2
	d dd 3
	e dd ?

.CODE

main:
	mov eax, a
	mul b
	sub eax, d
	mov e, eax
	call exit
end main