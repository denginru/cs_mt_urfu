.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	n db 2		;степень двойки
	k dd 134	;само число, которое хотим поделить
.CODE

main:
	mov 		eax, k
	neg			eax
	mov			cl,  n
	shr			eax, cl
	mov			k, eax
	call 		exit
end main