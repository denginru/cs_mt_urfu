.486
.model flat, stdcall

include stdlib.inc
include stdio.inc
includelib msvcrt.lib

.DATA
  a dd -10
  s dd ?
  form db "Number %d squared is %d", 13, 10, 0
  
  b dd 1234
  s1 dd ?
  
.CODE

main:
  mov eax, a
  imul a
  
  mov s, eax
  
  push s
  push a
  push offset form
  call printf
  add esp, 12
  
  mov eax, b
  imul b
  
  mov s1, eax
  
  push s1
  push b
  push offset form
  call printf
  add esp, 12  
  
  call exit  
END main






