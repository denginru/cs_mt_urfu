.486
.model flat, stdcall

include stdlib.inc
include stdio.inc
includelib msvcrt.lib

.DATA
  a dd -10
  b dd 3000000000
  form1 db "Just a text %d, %u, %o, %x, %X", 13, 10, 0
  
  a1 dw -10
  b1 dw 60000
  form2 db "%hd %hu %ho %hx %hX %d %x", 13, 10, 0

.CODE

main:
  push a
  push a
  push a
  push a
  push a
  push offset form1
  call printf
  add esp, 24

  push b
  push b
  push b
  push b
  push b
  push offset form1
  call printf
  add esp, 24

  push a1
  push a1
  push a1
  push a1
  push a1
  push a1
  push a1
  push offset form2
  call printf
  add esp, 18

  push b1
  push b1
  push b1
  push b1
  push b1
  push b1
  push b1
  push offset form2
  call printf
  add esp, 18
  
  call exit  
END main






