.486
.MODEL Flat, StdCall 

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	a 		dw 		?
	b 		dd 		0
	l 		dw 		?
	max 	dw 		?
	ar 		dw 		1000 dup (?)
	form 	db 		"%d",0
	form2 	db 		13, 10, "%d %d", 0
.CODE

main:

;Читаем длину массива (Работает ВЕРНО)
	mov 	esi, 	offset ar
	
	push 	offset l
	push 	offset form

	call 	scanf

	add 	esp, 	8
	mov 	ecx, 	0
	mov 	cx, 	l 	;Заносим длину массива в ecx - нужно для цикла

;Читаем числа (Работает верно!!!)
	mov 	edi, 	offset ar
	@read_numbers:
		push	ecx
		push 	offset a
		push	offset form
		call 	scanf
		add 	esp, 	8
		pop 	ecx
		mov 	ax, 	a
		mov 	[edi], 	ax
		add 	edi, 	type ar
	loop	 @read_numbers

;Ищем максимум
	mov 	eax, 	0
	push 	dword ptr l
	push 	offset ar
	call 	@find_max
	mov 	max, 	ax

;Печатаем числа
	mov 	esi, 	offset ar
	mov 	ecx, 	0
	mov 	cx, 	l
	mov 	eax, 	0
	@write_numbers:
		push 	ecx
		mov 	ax, 	[esi]
		push	eax
		sub 	ax, 	max
		push 	eax
		push 	offset 	form2
		call 	printf
		add 	esp, 	12
		add 	esi, 	type ar
		pop 	ecx
	loop 	@write_numbers

	call 	exit

	; Входные параметры:
;   esi - адрес начала массива (32-битные элементы)
;   eax - длина массива
; Выходные параметры:
;   eax - индекс максимума

@find_max:
  push ebp
  mov ebp, esp
  
; спасаю регистры
  push ecx
  push edx
  push esi

  mov esi, [ebp+8]
  mov ecx, [ebp+12]
  mov eax, 0
  mov edx, [esi]
  mov ebx, 1
  
@loop:
  cmp ebx, ecx
  je @ret
  cmp edx, [esi + 4*ebx]
  jge @do_nothing
  
  mov edx, [esi + 4*ebx]
  mov eax, ebx
  
@do_nothing:
  inc ebx
  jmp @loop

@ret:
; восстанавливаю регистры
  pop esi
  pop edx
  pop ecx
  
  pop ebp
  ret 

end 	main