.486
.model flat, stdcall

include stdlib.inc
include stdio.inc
includelib msvcrt.lib

.DATA
  mess db "Dear user! Please, enter a number: ", 0
  
  input db "%d", 0
  
  a dd ?
  s dd ?
  form db 13, 10, "Number %d squared is %d", 13, 10, 0
  
.CODE

main:
;  вывожу приглашение
  push offset mess
  call printf
  add esp, 4

;  ввожу число
  push offset a
  push offset input
  call scanf
  add esp, 8

;  вычислЯю  
  mov eax, a
  imul a  
  mov s, eax
  
;  вывожу результат  
  push s
  push a
  push offset form
  call printf
  add esp, 12  
  
  call exit  
END main






