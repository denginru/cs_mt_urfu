.486
.MODEL Flat, StdCall 

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	a 			dd 		?
	b 			dd 		?
	form_in 	db 		"%d%d",0
	form_out 	db 		13,10,"%d",0
.CODE

main:
	push	offset 	b
	push	offset 	a
	push 	offset 	form_in

	call 	scanf
	add 	esp, 	12

	mov 	eax, 	a
	mov 	ebx, 	b

	call 	nod

	push 	eax
	push 	offset form_out
	call 	printf
	add 	esp, 	8

	call 	exit


nod:
  cmp ebx, 0
  je stop

  cdq
  idiv ebx
  mov eax, ebx
  mov ebx, edx
  jmp nod
stop:
	cmp 	eax, 	0
	jge 	break
	neg 	eax
	break:
	ret
end main