.486
.model flat, stdcall

include stdlib.inc
include stdio.inc
includelib msvcrt.lib

.DATA
  MAX EQU 10
  
  mess db "Pythagoras' table 10 x 10:", 13, 10, 0
  
  form_num db " %-4d", 0
  form_endl db 13, 10, 0
  
.CODE

main:
;  вывожу заголовок
  push offset mess
  call printf
  add esp, 4

;  ebx - номер строки  
  mov ebx, 1  
@loop1:  
  cmp ebx, MAX
  ja @stop
  
;  ecx - номер столбца  
  mov ecx, 1 
@loop2:
  cmp ecx, MAX
  ja @end_loop2

  mov eax, ecx
  mul ebx

  pusha  
  push eax
  push offset form_num
  call printf
  add esp, 8
  popa

  inc ecx
  jmp @loop2  
  
@end_loop2:
  pusha
  push offset form_endl
  call printf   
  add esp, 4
  popa
  
  inc ebx
  jmp @loop1

@stop:  
  call exit  
END main






