.486
.MODEL Flat, StdCall 

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	a 		dd 		?
	form 	db 		"%u",0
	form1 	db 		13,10,"%u",13,10,0
.CODE

main:
	push 	offset 	a
	push 	offset 	form
	call 	scanf

	add 	esp, 	8

	mov 	eax, 	a
	add 	eax, 	10

	mov 	a, 		eax

	push 	a
	push 	offset 	form1
	call 	printf
	add 	esp, 	8
	call 	exit

end main