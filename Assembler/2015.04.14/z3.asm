.486
.MODEL Flat, StdCall 

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	a 		dw 		?
	l 		dw 		?
	max 	dw 		?
	ar 		dw 		1000 dup (?)
	form 	db 		"%d",0
	form2 	db 		13, 10, "%d %d", 0
.CODE

main:
	mov 	esi, 	offset ar
	
	push 	offset l
	push 	offset form

	call 	scanf

	add 	esp, 	8
	mov 	ecx, 	0
	mov 	cx, 	l


	; Вводим массив чисел
	@input_numbers:
		push 	ecx
		push 	offset a
		push	offset form
		call 	scanf
		add 	esp, 	8
		mov 	ax, 		a
		mov 	[esi], 		ax
		add 	esi, 		type ar
		pop 	ecx
		loop 	@input_numbers

	; Ищим максимум	
	mov 	esi, 	offset ar
	mov 	ax, 	l
	;call 	@find_max
	mov 	l, 		ax
	;call 	exit
	;выводим в соответствующием формате
	mov 	cx, 	l
	mov 	esi, 	offset ar
	@write_ar:
		mov 	eax, 	[esi]
		push 	ecx
		push 	eax
		sub 	ax, 	max
		push 	eax
		push 	offset form2
		call 	printf
		add 	esp, 	12
		pop 	ecx
		loop 	@write_ar

		call 	exit

; Входные параметры:
;   esi - адрес начала массива (32-битные элементы)
;   eax - длина массива
; Выходные параметры:
;   eax - индекс максимума
@find_max:
; спасаю регистры
  push ecx
  push edx

  mov ecx, eax
  mov eax, 0
  mov edx, [esi]
  mov ebx, 1
  
@loop:
  cmp ebx, ecx
  je @ret
  cmp edx, [esi + 4*ebx]
  jge @do_nothing
  
  mov edx, [esi + 4*ebx]
  mov eax, edx
  
@do_nothing:
  inc ebx
  jmp @loop

@ret:
; восстанавливаю регистры
  pop edx
  pop ecx
  
  ret  

end main