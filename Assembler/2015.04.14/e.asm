.486
.model flat, stdcall

include stdlib.inc
include stdio.inc
includelib msvcrt.lib

.DATA
  mess db "You pressed the symbol '%c' with the code %d", 13, 10, 0
  
.CODE

main:
@loop:
  call getch
  
  pusha  
  push eax
  push eax
  push offset mess
  call printf
  add esp, 12
  popa
  
  cmp eax, 27
  jne @loop

  call exit  
END main






