.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  fa dq ?
  fb dq ?
  fc dq ?
  fd dq ?
  fres dq ?
  mess db "Enter 4 real numbers: ", 0
  endl db 13, 10, 0
  inform db "%lf%lf%lf%lf", 0
  mess1 db "%lf*%lf - %lf/%lf = %.12lf", 13, 10, 0
     
.CODE
main:
;  Печать приглашения
  push offset mess
  call printf 
  add esp, 4
  
;  Собственно ввод  
  push offset fd
  push offset fc
  push offset fb
  push offset fa
  push offset inform
  call scanf
  add esp, 20
  
;  Вычисления
  finit
  fld fa
  fld fb
  fmul
  fld fc
  fld fd
  fdiv
  fsub
  fstp fres
  
;  Первод строки после ввода  
  push offset endl
  call printf
  add esp, 4
  
;  Печать результата  
  push dword ptr [fres+4] 
  push dword ptr fres    

  push dword ptr [fd+4] 
  push dword ptr fd    
  
  push dword ptr [fc+4] 
  push dword ptr fc    

  push dword ptr [fb+4] 
  push dword ptr fb    
  
  push dword ptr [fa+4] 
  push dword ptr fa
  
  push offset mess1
  call printf
  add esp, 44
  
  call exit    
END main
  


  
  
  
  