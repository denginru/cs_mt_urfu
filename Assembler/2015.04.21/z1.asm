.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  a 	 	dq ?
  b  	 	dq ?
  x 		dq ?
  mess   	db "Enter two real numbers (a and b for solving ax+b=0): ", 0
  mess_ans 	db "x=%lf",13,10, 0
  endl	 	db 13, 10, 0
  inform 	db "%lf %lf", 0
;  mess1 db "You have entered %g", 13, 10, 0
     
.CODE
main:
	push 	offset mess
	call 	printf
	add 	esp, 	8

	push	offset b
	push 	offset a
	push 	offset inform
	call 	scanf
	add 	esp, 	12

	finit
	fld 	a
	fld 	b
	fdiv 	st, 	st(1)
	fchs
	fst 	x

	push	offset endl
	call 	printf 	
	add 	esp, 	4

	push 	dword ptr [x+4]
	push 	dword ptr x
	push  	offset mess_ans
	call 	printf
	add 	esp,	 12

	call 	exit

end main