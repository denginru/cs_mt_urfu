.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  fnum1 dq ?
  fnum2 dq ?
  mess db "Enter two real numbers: ", 0
  endl db 13, 10, 0
  inform db "%lf%lf", 0
  mess1 db "The first number is greater", 13, 10, 0
  mess2 db "The second number is greater", 13, 10, 0
     
.CODE
main:
;  Печать приглашения
  push offset mess
  call printf 
  add esp, 4
  
;  Собственно ввод  
  push offset fnum2
  push offset fnum1
  push offset inform
  call scanf
  add esp, 12
  
;  Первод строки после ввода  
  push offset endl
  call printf
  add esp, 4

;  Вычисления
  finit
  fld fnum2
  fld fnum1
  
  fcom
  fstsw ax
  sahf
  
  jb @1st_less  ;  БЕЗЗНАКОВЫЙ ПЕРЕХОД!!!
  
;  Печать "первое больше"
  push offset mess1
  call printf
  add esp, 4
  jmp @exit

;  Печать "второе больше"
@1st_less:
  push offset mess2
  call printf
  add esp, 4
  jmp @exit
  
@exit:  
  call exit    
END main
  


  
  
  
  