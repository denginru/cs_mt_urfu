.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  fnum dq ?
  mess db "Enter a real number: ", 0
  endl db 13, 10, 0
  inform db "%lf", 0
;  mess1 db "You have entered %g", 13, 10, 0
  mess1 db "You have entered %lf", 13, 10, 0
     
.CODE
main:
;  Печать приглашения
  push offset mess
  call printf 
  add esp, 4
  
;  Собственно ввод  
  push offset fnum
  push offset inform
  call scanf
  add esp, 8
  
;  Первод строки после ввода  
  push offset endl
  call printf
  add esp, 4
  
;  Печать результата  
  push dword ptr [fnum+4] 
     ; кладу в стек старшую часть числа
  push dword ptr fnum    
     ; кладу в стек младшую часть числа
  push offset mess1
  call printf
  add esp, 12
  
  call exit    
END main
  


  
  
  
  