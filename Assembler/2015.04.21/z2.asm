.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  a 	 	dq ?
  b  	 	dq ?
  mess   	db "Enter two real numbers: ", 0
  mes_max 	db "Max number is %lf",13,10, 0
  mes_min 	db "Min number is %lf", 13,10,0
  endl	 	db 13, 10, 0
  inform 	db "%lf %lf", 0
;  mess1 db "You have entered %g", 13, 10, 0
     
.CODE
main:
	push  	offset	 	mess
	call 	printf
	add 	esp, 		4

	push 	offset 		b
	push 	offset 		a
	push 	offset		inform
	call 	scanf
	add 	esp, 		8

	push offset endl
	call printf
	add esp, 4

;	push 	dword ptr [b+4]
;	push 	dword ptr b
;	push 	dword ptr [a+4]
;	push 	dword ptr a
;	push	offset inform
;	call 	printf

	finit
	fld 	b
	fld 	a
	fcom
	fstsw 	ax
	sahf
	jb 	@max_b


	@max_a:
		push 	dword ptr [a+4]
		push	dword ptr a
		push 	offset mes_max
		call 	printf
		add 	esp, 	12

		push 	dword ptr [b+4]
		push	dword ptr b
		push 	offset mes_min
		call 	printf
		add 	esp, 	12
		jmp 	@exit

	@max_b:
		push 	dword ptr [b+4]
		push	dword ptr b
		push 	offset mes_max
		call 	printf
		add 	esp, 	12

		push 	dword ptr [a+4]
		push	dword ptr a
		push 	offset mes_min
		call 	printf
		add 	esp, 	12


	@exit:
		call 	exit
end main