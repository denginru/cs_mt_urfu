.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  a 	 	dq ?
  b  	 	dq ?
  cc 		dq ?
  d 		dq ?
  x1 		dq ?
  x2 		dq ?
  x 		dq ?
  f 		dq 4.0
  z 		dq 0.0
  two 		dq 2.0
  mess   	db "Enter three real numbers (a, b and c for solving ax^2+bx+c=0): ", 0
  ans 	 	db 13, 10, "x1 = %lf",13,10,"x2 = %lf", 0
  no_ans	db 13, 10, "No real roots"
  endl	 	db 13, 10, 0
  inform 	db "%lf%lf%lf", 0
  outf 		db 13, 10, "%lf"
;  mess1 db "You have entered %g", 13, 10, 0
     
.CODE
main:
; Приглашение на ввод
	push 	offset 	mess
	call 	printf
	add 	esp, 	4
; Ввод
	push 	offset 		cc
	push 	offset 		b
	push 	offset 		a
	push 	offset		inform
	call 	scanf
	add 	esp, 		16
; Вычисляем дискриминант
	finit
	fld 	b				;st0 = b
	;--------------------------------------
	fmul 	st(0), st(0) 	;st0 = b^2
	fstp 	st(1) 			;st3 = st0 = b^2
	;--------------------------------------
	fld 	a 				;st0 = a
	fchs					;st0 = -a
	fld 	f 				;st1 = 4
	fld 	cc 				;st2 = c
	;--------------------------------------
	fmul 	st(0), st(1) 	;st0 = 4c
	fmul 	st(0), st(2) 	;st0 = -4ac
	fadd 	st(0), st(3)	;st0 = -4ac+b^2
	fst 	d 				;Запоминаем дискриминант

	fcom 	z
	fstsw 	ax
	sahf
	jb 		@no_roots

@exist_roots:
	; Ищем первый корень
	fsqrt
	fld 	b
	fchs
	fadd 	st(0), st(1)
	fld 	two
	fld 	a
	fmul 	st(0), st(1)
	fdivr 	st(0), st(2)
	fst 	x1
	; Ищем второй корень
	finit
	fld 	d
	fsqrt
	fld 	b
	fchs
	fsub 	st(0), st(1)
	fld 	two
	fld 	a
	fmul 	st(0), st(1)
	fdivr 	st(0), st(2)
	fst 	x2
	;Выводим на печать
	push	dword ptr [x2+4]
	push	dword ptr x2
	push	dword ptr [x1+4]
	push	dword ptr x1
	push  	offset ans
	call 	printf
	add 	esp, 24
	call 	@exit
@no_roots:
	push 	offset no_ans
	call 	printf
	add 	esp, 	4
@exit:
	call 	exit
end main