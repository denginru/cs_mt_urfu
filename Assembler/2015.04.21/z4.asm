.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  n 	 	dd 		?
  mess 		db 		"Enter an integer number: ",0
  inf 		db 		"%u",0
  res 		dq 		0.0
  outf 		db 		13, 10, "res = %lf",0
  one 		dq 		1.0
     
.CODE
main:
	push	 offset mess
	call 	 printf
	add 	 esp, 	4

	push	offset n
	push	offset inf
	call 	scanf
	add 	esp,	8

	@loop:
		finit
		fild 	n
		fmul 	st(0), 	st(0)
		fld 	one
		fdiv 	st(0), st(1)
		fld 	res
		fadd 	st(0), st(1)
		fst 	res
		dec 	n
		cmp 	n, 	0
		jne 	@loop

	push 	dword ptr [res+4]
	push	dword ptr res
	push	offset outf
	call 	printf
	add 	esp, 	12

	call  	exit

end main