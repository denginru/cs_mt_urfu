.486
.MODEL Flat, StdCall
INCLUDE stdio.inc
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
  fnum1 dq ?
  fnum2 dq ?
  fnum3 dq ?
  mess db "Enter two real numbers: ", 0
  endl db 13, 10, 0
  inform db "%lf%lf", 0
;  mess1 db "%lf + %lf = %lf", 13, 10, 0
  mess1 db "%g + %g = %g", 13, 10, 0
     
.CODE
main:
;  Печать приглашения
  push offset mess
  call printf 
  add esp, 4
  
;  Собственно ввод  
  push offset fnum2
  push offset fnum1
  push offset inform
  call scanf
  add esp, 12
  
;  Вычисления
  finit
  fld fnum1
  fld fnum2
  fadd
  fstp fnum3
  
;  Первод строки после ввода  
  push offset endl
  call printf
  add esp, 4
  
;  Печать результата  
  push dword ptr [fnum3+4] 
  push dword ptr fnum3    

  push dword ptr [fnum2+4] 
  push dword ptr fnum2    
  
  push dword ptr [fnum1+4] 
  push dword ptr fnum1    

  push offset mess1
  call printf
  add esp, 28
  
  call exit    
END main
  


  
  
  
  