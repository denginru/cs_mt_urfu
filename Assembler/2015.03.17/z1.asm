.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len 	EQU		 4
	v1 		dd 		 5, 2, 4, 3
	v2 		dd 		 3, 4, 2, 5
	r		dd		 len dup (1)
	len2	dd 		 ?
	sp_		dd 		 ?
 
.CODE

main:
	;mov 	ecx, 	len
	mov 	ecx, 	offset r
	mov 	esi, 	offset v1
	mov 	edi, 	offset v2
	mov 	ebx, 	0
	mov 	len2, 	len

@loop:

;eax - считаем умножение
;ebx - копим скалярное произведение
;ecx - индекс
;ecx - копим сумму векторов

;Тут считаем скалярное произведение
	mov 	eax, 	[esi]
	mul 	dword ptr [edi]
	add 	ebx, 	eax

;Тут считаем сумму векторов - это просто сумма векторов
	mov 	eax, 	[esi]
	add 	eax,	[edi]
	mov 	[ecx], 	eax
;Тут пересчитываем индексы
	add 	esi, 	type v1
	add 	edi, 	type v2
	add 	ecx, 	type r
	dec 	len2
	cmp 	len2,	0
	jne 	@loop

	mov 	sp_, 	ebx
	call exit
end main