.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
  len EQU 5
  ar dd 0, -5, 10, 20, -3

.CODE

; можно тут

main:
; можно тут - но плохо
  mov ecx, len

@repeat:  
  mov eax, ecx
  mov esi, offset ar
  call @find_max
  
  dec ecx
  mov ebx, [ar + 4*ecx]
  mov edx, [ar + 4*eax]
  mov [ar + 4*ecx], edx
  mov [ar + 4*eax], ebx
  
  cmp ecx, 0
  jne @repeat
  
  call exit

; можно тут

; Входные параметры:
;   esi - адрес начала массива (32-битные элементы)
;   eax - длина массива
; Выходные параметры:
;   eax - индекс максимума
@find_max:
; спасаю регистры
  push ecx
  push edx

  mov ecx, eax
  mov eax, 0
  mov edx, [esi]
  mov ebx, 1
  
@loop:
  cmp ebx, ecx
  je @ret
  cmp edx, [esi + 4*ebx]
  jge @do_nothing
  
  mov edx, [esi + 4*ebx]
  mov eax, ebx
  
@do_nothing:
  inc ebx
  jmp @loop

@ret:
; восстанавливаю регистры
  pop edx
  pop ecx
  
  ret  

END main






