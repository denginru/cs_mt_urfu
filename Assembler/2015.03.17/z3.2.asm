.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len 	dw		 0
	digs	db 		 12 dup (0)
	digsr 	db 		 12 dup (0)
	;n 		dd 		 1267
	;n 		dd 		 0
	n 		dd 		 -127
	d 		dd 		 10	

.CODE
main:
	mov 	esi, 	offset digs
	mov 	edi,	offset digsr
	mov 	eax, 	n
	cmp 	eax, 	0
	jle 	@loop
	neg 	eax
	
	@loop:
		mov 	edx, 	0
		div 	d
		mov 	[esi], 	dl
		add 	esi, 	type digs
		inc 	len
		cmp 	eax, 	0
		jne 	@loop

	call 	exit
end main