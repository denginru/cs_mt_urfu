.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
len 	equ 	4
ar 		dd 		4, 3, 6, 5

.CODE
main:
mov 	ecx, 	len
mov 	eax, 	ecx
mov 	esi, 	offset ar
call 	@find_max
dec 	ecx
mov 	ebx, 			[ar+4*ecx]
mov 	edx, 			[ar+4*eax]
mov 	[ar+4*ecx],		edx
mov 	[ar+4*eax], 	ebx
call 	exit

;Входные параметры:
;  esi - адрес начала массива
;  eax - Длина массива
;Выходные параметры:
;  eax - индекс массива

@find_max:
	;Спасаем регистры
	push 	edx
	push	ecx

	mov 	ecx, 	eax
	mov 	eax, 	0
	mov 	edx, 	[esi+4*eax]
	mov 	ebx, 	1
	@loop:
		cmp 	ebx, 	ecx
		je 		@ret
		cmp 	edx, 	[esi + 4*ebx]
		jge 	@do_nothing

		mov 	edx, 	[esi+4*ebx]
		mov 	eax, 	ebx
		@do_nothing:
			inc 	ebx
			jmp 	@loop
	@ret:
		;восстанавливаем регистры
		pop 	ecx
		pop		edx
		ret
end main