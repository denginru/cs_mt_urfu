.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len 	EQU		 4
	ar 		dd 		 5, 2, 4, 3
	thr 	dd 		 7
	res		dd 		 0FFFFFFFFH

.CODE
main:
	mov 	esi, 	offset ar
	mov 	eax, 	thr
	mov 	ecx, 	len
	@loop:
		cmp 	[esi], 		eax
		jg 		@good_exit
		add 	esi, 		type ar
		loop @loop
	call exit
	@good_exit:
		mov 	eax, 	[esi]
		mov 	res, 	eax
		call 	exit
end main