.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len 	dw		 0
	digs	db 		 12 dup (0)
	digsr 	db 		 12 dup (0)
	n 		dd 		 1267
	d 		dd 		 10	

.CODE
main:
	mov 	esi, 	offset digs
	mov 	edi,	offset digsr
	mov 	eax, 	n
	@loop:
		inc 	len
		cmp 	eax, 	0
		jnb		@good
		neg 	eax
		@good:
		div 	d
		mov 	[edi], 	dh
		cmp		ebx,	0
		jne 	@loop
	
	mov 	ecx, 	0
	mov 	cx, 	len
	@loop2:
		mov 	[edi], 	ah
		mov 	ah, 	[esi]
		sub		edi, 	type digsr
		add 	esi,	type digs
	loop 		@loop
	
	call exit
end main