.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
a dd 7
b dd -3
cd dd ?
.CODE
main:
mov eax, a
mov ebx, b
cmp eax, ebx
jge tr
mov cd, ebx
jmp ex
tr: 
mov cd , eax 
ex:
call exit
end main