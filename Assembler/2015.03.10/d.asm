.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
  len EQU 6

  ar1 dw 1, 2, 3, 5, -1, -10

  val dd 19

.CODE
;  6 <- äëèíà ìàññèâà
main:
  mov esi, offset ar1
  mov edx, 0

  inc val

@loop:
;  mov ax, [esi + type ar1*edx]
;  inc ax
;  mov [esi + type ar2*edx], ax
  inc word ptr [esi + type ar1*edx]

  inc edx
  cmp edx, len
  jb @loop

  call exit
end main












