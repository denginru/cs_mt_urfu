.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	n equ 10
	i dd 1
	a dd ?
.CODE

main:
	mov eax, 1
	@loop:
		mul i
		inc i
		cmp i, n
		jbe @loop		
	mov a, eax
	call exit
end main
