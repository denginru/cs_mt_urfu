.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
;  len dd 6     Плохое, негодное определение
  len EQU 6

  ar1 dw 1, 2, 3, 5, -1, -10
  ar2 dw len dup(?) 

.CODE
;  6 <- длина массива
main:
  mov esi, offset ar1
  mov edi, offset ar2
  mov ecx, len

@loop:
  mov ax, [esi]
  mov [edi], ax
  add esi, type ar1
  add edi, type ar2
  loop @loop


  call exit
end main
