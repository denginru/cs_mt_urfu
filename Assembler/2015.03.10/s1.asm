.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
  a dd 10
  b dw -15

.CODE

main:
  push a
  
  mov bx, b
  push bx

;  Извлекаем в порядке, обратном укладке
  pop dx
  pop ecx

  push word ptr 20
  pop cx

  mov eax, offset a
  push dword ptr [eax]

  pop ecx

  call exit
end main
