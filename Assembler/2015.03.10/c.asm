.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
;  len dd 6     Плохое, негодное определение
  len EQU 6

  ar1 dw 1, 2, 3, 5, -1, -10
  ar2 dw len dup(?) ; <- ошибка

.CODE
;  6 <- длина массива
main:
  mov esi, offset ar1
  mov edi, offset ar2
  mov edx, 0

@loop:
  mov ax, [esi + type ar1*edx]
  mov [edi + type ar2*edx], ax
  inc edx
  cmp edx, len
  jb @loop

  call exit
end main












