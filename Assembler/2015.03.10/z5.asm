.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len		EQU 4
	ar1		db 5, 12, 4, 8
	ar2 	db len dup (?)
.CODE

main:
	mov 	esi, 	offset ar1
	mov 	edi, 	offset ar2 + (type ar2) * (len-1)
	mov 	ecx, 	len
	@loop:
		mov 	al, 	[esi]
		mov 	[edi], 	al
		sub 	edi, 	type ar2
		add 	esi, 	type ar1
		loop 	@loop
	call exit
end main