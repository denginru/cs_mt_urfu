.486

.MODEL Flat, StdCall

INCLUDE 			stdlib.inc
INCLUDELIB 			msvcrt.lib

.DATA

	;year 	dw 		2012	да
	;year 	dw 		2015	нет
	;year 	dw 		2016	да
	;year 	dw 		200 	нет
	year 	dw 		2000
	a 		dw 		400
	b 		dw 		4
	d 		dw 		100
.CODE

main:
	mov 	eax, 	0
	mov 	ax, 	year
	cdq
	div 	a
	cmp 	dx, 	0
	je 		@exit_good

	mov 	ax, 	year
	cdq
	div 	b
	cmp 	dx, 	0
	jne 	@exit_bad

	mov 	ax, 	year
	cdq
	div 	d
	cmp 	dx, 	0
	jne 	@exit_good

	@exit_good:
		mov 	ax, 	1
		jmp 	@exit
	@exit_bad:
		mov 	ax, 	0
	@exit:
		call exit
end main