.486

.MODEL Flat, StdCall

INCLUDE 			stdlib.inc
INCLUDELIB 			msvcrt.lib

.DATA
	len		EQU 	4
	ar1		dw 		1, -23, 15, 137
	ar2		db		len dup (?)
.CODE

main:
	
	mov 	esi, 	offset ar1
	mov 	edi, 	offset ar2
	mov 	ecx, 	len
	mov 	eax, 	0
	mov 	bl, 	10

	

	@loop:
		mov 	ax, 	[esi]
		cmp 	ax, 0
		jge 	@lexit
		neg ax
	@lexit:
		xor dx, dx
		div 	bl
		mov 	[edi], 	ah
		add 	esi, 	type ar1
		add 	edi, 	type ar2
		loop 	@loop
	call exit
end main