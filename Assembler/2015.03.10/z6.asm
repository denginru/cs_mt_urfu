.486

.MODEL Flat, StdCall

INCLUDE 			stdlib.inc
INCLUDELIB 			msvcrt.lib

.DATA
	len		EQU 	4
	ar1		dd 		1, -23, 15001, 137
	ar2		dd 		len dup (0)
	k 		dd 		5
.CODE

main:
	mov 	esi, 	offset ar1
	mov 	edi, 	offset ar2
	mov 	ecx, 	len
	
	@loop:
		mov 	eax, 	[esi]
		cdq
		idiv 	k
		cmp 	edx, 	0
		jne		@go		
		mov 	ebx, 	[esi]
		mov 	[edi], 	ebx
		add 	edi, 	type ar2
		@go:
		add 	esi, 	type ar1
		loop 	@loop
	call 	 exit
end main