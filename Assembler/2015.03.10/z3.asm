.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len		EQU 4
	ar		dw 5, 12, 4, 8
	sum		dw 0
	pr		dw ?
.CODE

main:
	mov esi, offset ar
	mov eax, 1
	mov ebx, 0
	mov ecx, len
	cdq
	@loop:
		mul word ptr [esi]
		add bx, [esi]
		add esi, type ar
		loop @loop
	mov sum, bx
	mov pr,  ax
	call exit
end main