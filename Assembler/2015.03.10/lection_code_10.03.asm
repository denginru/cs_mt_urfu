.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	len EQU 6	;const
	ar1 dw 1,2,3,5,-1,-10
	ar2 dw 6 dup (?)
.CODE

main:
	mov esi, offset ar1
	mov edi, offset ar2
	mov ecx, len

	@loop:
		mov ax, [esi]
		mov [edi], ax
		add esi, type ar1
		add edi, type ar2
		loop @loop

	call exit
end main
