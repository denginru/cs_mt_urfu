.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd -145
	b dd 3
	res dd ?
.CODE

main:
	mov 	eax, a
	mov		edx, 0
	cdq
	idiv	b
	mov		res, edx
	call 	exit
end main