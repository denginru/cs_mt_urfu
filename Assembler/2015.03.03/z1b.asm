.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dw 5
.CODE

main:
	mov ax, 0
	sub ax, a
	mov a, ax
	call exit
end main