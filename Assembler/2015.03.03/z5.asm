.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd 486
	b dd 2
	d dd 3
	e dd 1

.CODE

main:
	mov eax, a
	mov bl, 2
	div bl
	cmp edx, 0
	je @eve
		mul d
		add eax, 1
		mov a, eax
		jmp @after
	@eve:
		div b
		mov a, eax
	@after:
		call exit
end main