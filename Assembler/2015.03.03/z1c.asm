.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd 5
.CODE

main:
	mov eax, 0
	sub eax, a
	mov a, eax
	call exit
end main