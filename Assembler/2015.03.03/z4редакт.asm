.486

.MODEL Flat, StdCall

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib

.DATA
	a dd 100000
	b dd 100000
	d dd 17

.CODE
main:
	mov eax, a
	mul b
	div d
	call exit
end main