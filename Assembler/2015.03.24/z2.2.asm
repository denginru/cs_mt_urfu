.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	a 		dd 		2
	res 	dd 		?
	len 	equ 	3
	p 		db 		1, 2, 3

.CODE

main:
	mov 	edx, 	0
	mov 	ecx, 	len
	mov 	esi, 	offset p
	
	@loop:
	mov 	bl, 	[esi]
	mov 	eax, 	1
	call 	@pow
	add 	edx, 	eax
	add 	esi, 	type p
	loop 	@loop
	mov 	res, 	edx
	call 	exit

	@pow:
	push 		edx
	;dec 		bl	
	@loop2:
		cmp 	bl,		0
		je 		@stop
		mul 	a
		dec 	bl
		jmp		@loop2
	@stop:
	pop edx
	ret

end 	main 