; !!! ОБЛАЖАЛСЯ!!!  ИЗ EBP нельзя вычитать!
; Исправлюсь на следующей паре...  ;)

.486
.model flat, stdcall

include stdlib.inc
includelib msvcrt.lib

.DATA
  len EQU 11
  ar dd 1, 3, 1, 2, 3, 1, 2, 2, 3, 1, 2

.CODE

main:
  mov ecx, len

@repeat:  
  push ecx
  push offset ar
  call @Lin_sort
  add esp, 8
    
  call exit


; Процедура линейной сортировки
; Входные параметры:
;   [ebp+8]  - адрес начала массива (32-битные элементы)
;   [ebp+12] - длина массива (32-битное беззнаковое целое)
; Выходные параметры:
;   нет
; Локальные переменные:
;   [ebp-4]  - количество единиц
;   [ebp-8]  - количество двоек
;   [ebp-12] - количество троек
@Lin_sort:
  push ebp
  mov ebp, esp
  
; !!! отвожу пространство для локальных переменных
  sub esp, 12  
  
; спасаю регистры
  push eax
  push ecx
  push esi

  mov esi, [ebp+8]
  mov ecx, [ebp+12]
  
  mov [ebp-4], dword ptr 0     ; !!!
  mov [ebp-8], dword ptr 0     ; !!! Здесь были глюки - не указал размер числа
  mov [ebp-12], dword ptr 0    ; !!!
  
; цикл подсчета  
@loop:
  mov eax, [esi+4*ecx-4]
  neg eax                      ; !!! Нельзя написать [ebp-4*eax], можно только [ebp+4*eax]
  inc dword ptr [ebp+4*eax]    ; !!! Поэтому сначала изменяю знак eax, а потом пишу [ebp+4*eax]
  loop @loop
  
; заполняю единицы
  mov ecx, [ebp-4]
fill1:
  cmp ecx, 0
  je stop1  
  mov [esi], dword ptr 1
  add esi, 4
  dec ecx
  jmp fill1
  
stop1:
; заполняю двойки
  mov ecx, [ebp-8]
fill2:
  cmp ecx, 0
  je stop2
  mov [esi], dword ptr 2
  add esi, 4
  dec ecx
  jmp fill2

stop2:
; заполняю тройки
  mov ecx, [ebp-12]
fill3:
  cmp ecx, 0
  je stop3
  mov [esi], dword ptr 3
  add esi, 4
  dec ecx
  jmp fill3
  
stop3:
; восстанавливаю регистры
  pop esi
  pop ecx
  pop eax  
  
; очищаю область локальных переменых
  add esp, 12
  
  pop ebp
  ret  

END main






