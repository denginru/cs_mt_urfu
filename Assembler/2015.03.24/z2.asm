.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	a 		dd 		4
	res 	dd 		?
	len 	equ 	3
	p 		db 		1, 2, 3

.CODE

main:
	mov 	eax, 	a
	mov 	edx, 	0
	mov 	ecx, 	len
	mov 	esi, 	offset p
	@loop:
		mov 	bl ,	[esi]
		push 	eax
		call 	@pow
		add 	edx, 	eax
		pop 	eax
		add 	esi, 	type p
		loop 	@loop
	mov 	res, 	edx
	call exit

@pow:
	push 		edx
	dec 		bl
	@loop2:
		mul 	a
		dec 	bl
		cmp 	bl, 	0
		jg		@loop2
	pop 		edx
	ret

end main