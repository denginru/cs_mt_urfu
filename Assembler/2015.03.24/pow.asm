.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	a 		dd 		5
	s 		db 		6
.CODE

main:
	mov 	eax, 	a
	dec 	s
	@loop:
		mul 	a
		dec 	s
		cmp 	s ,		0
		jg 		@loop
	mov 	a ,		eax
	call 	exit
end main