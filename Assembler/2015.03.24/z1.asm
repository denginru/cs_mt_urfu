.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
; массив нулевой длины не рассматриваем
;len 	equ 	1
;len 	equ 	2
;len 	equ 	3
	len 	equ 	4
	badlen	dd 		1
	num 	dd 		28, -70, 154, 98

.CODE

main:
	mov 	esi, 	offset num
	mov 	eax, 	[esi]	;в eax кладём 1 элемент массива (с чего-то нужно начинать)
	mov 	ecx, 	len-1	;соответственно элементов к рассматрению на 1 меньше
	add 	esi,	type num;переходим к следующему эл-ту (начиная с него будем элементы засылать в ebx)
	;Берём модуль
	cmp	 	eax, 	0
	jnb 	start
	neg 	eax
	start:
	;Если вдруг массив состоит из 1 числа, то рез-т abs(само число) (оно уже в eax)
	cmp 	badlen, 	len
	je 		halt	;поэтому выходим из программы
	;если в массиве больше 1 числа делаем цикл
	;в цикле в eax будем хранить текущий НОД (в конце работы программы там будет лежать конечный ответ)
	@loop:
		mov 	ebx, 	[esi]
		call 	@nod
		add 	esi, 	type num
		loop 	@loop
	halt:
	call 	exit

;eax и ebx - 2 числа НОД которых нужно найти
@nod:
		cmp 	ebx, 	0
		jge 	del
		neg 	ebx
	;Этот цикл сам алгоритм Евклида
	@loop2:
		;В начале смотрим какое из чисел больше - то и должно лежать в eax (другое, соответственно, в ebx)
		del:
		cmp 	ebx, 	eax
		jbe 	good	;Если менять местами их не нужно, то идём дальше
		push 	ebx
		mov 	ebx ,	eax
		pop 	eax

		good:
		mov 	edx, 	0	;зачищаем edx
		div 	ebx
		cmp 	edx, 	0	;если получили нулевой остаток, то НОД данных 2 чисел найден
		je 		exit_nod
		mov 	eax, 	edx	; Наибольшее из чисел (у нас это eax) заменяем остатком от деления
		jmp 	@loop2
		exit_nod:
		mov 	eax, 	ebx ; Нод - наименьшее из оставшихся чисел (у нас это ebx), но рез-т должен лежать в eax, кладём его туда
		ret
end main