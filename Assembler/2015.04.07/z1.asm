.486
.MODEL Flat, StdCall 

INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	l1 		equ 	4
	l2 		equ 	3
	v1 		dd 		1, 2, 4, 6
	v2 		dd 		5, 2, 3

.CODE

main:
	push 	v1
	push 	v2
	push 	l1
	push 	l2
	call 	exit;

@sum_vectors:
	push 	ebp
	mov 	ebp, 	esp
	sub 	esp, 	128

	@mul_vec_to_num:
		push 	ecx
		mov 	ecx, 	0

end main