.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib
INCLUDE stdio.inc

.DATA
	a 		dd	 	-10
	form1 	db 		"%d %u %o %x %X", 13, 10, 0

.CODE

main:
	push 	a
	push 	a
	push 	a
	push 	a
	push 	a
	push 	offset form1
	call 	printf
	add 	esp, 	24
	call 	exit



end main