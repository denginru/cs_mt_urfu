.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	n 		db		5
.CODE

main:
;	mov 	eax, 	0
;	mov 	ebx, 	0
	mov 	al, 	n
;	mov 	bl, 	n
;	cmp 	n, 		1
;	jbe		@zero_fact
;	dec 	bl
	call 	@fact
	@exit:
	call 	exit

	@zero_fact:
	mov 	eax, 	1
	call 	exit


; al - число, факториал которого считается

; eax - копимый факториал
; ebx - текущий множитель

@fact:
    push edx
	cmp 	ebx, 	1
	jbe 	@ret
	mul 	ebx
	dec 	ebx
	call 	@fact
	@ret:
	    pop edx
		ret


end main