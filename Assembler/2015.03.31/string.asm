.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	a 		db 		"hello world", 0
.CODE

main:
	mov 	esi, 	offset a
	call 	@length
	call 	exit
@length:
	mov 	eax, 	0
	@loop:
		cmp 	[esi+eax], 	byte ptr 0
		je 		@exit
		inc 	eax
		jmp		@loop
	@exit:
	ret
end main