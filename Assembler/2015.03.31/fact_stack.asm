.486
.MODEL Flat, StdCall 
INCLUDE stdlib.inc
INCLUDELIB msvcrt.lib


.DATA
	n 		dw		5
	zero 	dw 		0
.CODE

main:
	mov 	eax, 	0
	mov 	ax, 	n
	cmp 	n, 		1
	jbe		@zero_fact
	dec 	n
	call 	@fact
	@exit:
	call 	exit

	@zero_fact:
	mov 	eax, 	1
	call 	exit

	@fact:
	push 	ebp
	mov 	ebp,	 esp
	sub 	esp, 	 2
	mov 	[ebp-2], 		n
	cmp 	word ptr [ebp-2], 0
	je 		@ret
	mul	 	word ptr [ebp-2]
	dec 	n
	call 	@fact
	@ret:
	ret
end main